import container from "./container";
import routing from "../routing";

export default {
    container,
    routing
};
