/**
 * This is the app container. All app-wide data is
 * stored here. Any setting and getting should be
 * performed with the set and get methods.
 */
export default {
    routes: {},

    req: null,

    res: null,

    set(key, value) {
        this[key] = value;
    },

    get(key) {
        if (!this.hasOwnProperty(key)) {
            return null;
        }

        return this[key];
    },

    remove(key) {
        if (this.hasOwnProperty(key)) {
            delete this[key];
        }
    }
};
