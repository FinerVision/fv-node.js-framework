import {renderToString} from "react-dom/server";
import React from "react";
import app from "../app/app";

// TODO: Handle errors properly.
export default (req, res) => {
    // Check if requested url is in the app's routes
    if (!app.container.routes.hasOwnProperty(req.url)) {
        res.statusCode = 404;
        return res.end();
    }

    const route = app.container.routes[req.url];

    // Check if requested HTTP method is allowed
    if (!route.hasOwnProperty(req.method)) {
        res.statusCode = 405;
        return res.end();
    }

    res.statusCode = 200;

    let response = route[req.method].callable();

    try {
        response = renderToString(response);
    } catch (e) {
        if (!(response instanceof Buffer)) {
            response = JSON.stringify(response);
        }
    }

    if (response === undefined) {
        response = '';
    }

    res.write(response);

    return res.end();
};
