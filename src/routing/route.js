import app from '../app/app';

/**
 * Adds the given route's method, url and action to the
 * app container for future use on an incoming request.
 *
 * @param method
 * @param url
 * @param action
 */
const addRoute = (method, url, action) => {
    const routes = app.container.get('routes');
    routes[url] = {};
    routes[url][method] = {};

    if (typeof action === 'string') {
        action = action.split('@');
        const controller = require(app.container.get('paths').app + `/Controllers/${action[0]}`).default;
        const callable = action[1];
        routes[url][method].callable = () => (new controller)[callable]();
    }

    if (typeof action === 'function') {
        routes[url][method].callable = () => action();
    }

    app.container.set('routes', routes);
};

/**
 * All of the supported HTTP request methods are listed here. You can see a complete list of
 * methods here - https://en.wikipedia.org/wiki/Hypertext_Transfer_Protocol#Request_methods
 */
export default {
    get: (url, action) => addRoute('GET', url, action),
    post: (url, action) => addRoute('POST', url, action),
    put: (url, action) => addRoute('PUT', url, action),
    patch: (url, action) => addRoute('PATCH', url, action),
    delete: (url, action) => addRoute('DELETE', url, action)
};
