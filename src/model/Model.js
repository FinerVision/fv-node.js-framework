class Model {
    constructor() {
        this.fillable = [];
        this.protected = [];
    }
}

export default Model;
