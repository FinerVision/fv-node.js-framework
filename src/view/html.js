import path from "path";
import fs from "fs";
import app from "../app/app";

export default (pathString) => {
    const paths = app.container.get('paths');
    const filePath = path.join(...pathString.split('.')) + '.html';
    const file = paths.app + `/Views/${filePath}`;
    const res = app.container.get('res');

    if (!fs.existsSync(file)) {
        res.statusCode = 404;
        return null;
    }

    res.setHeader('Content-Type', 'text/html; charset=utf-8');

    return fs.readFileSync(file);
};
