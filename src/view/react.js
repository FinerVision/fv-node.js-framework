import {createElement} from "react";
import path from "path";
import fs from "fs";
import app from "../app/app";

const layout = (pathString, props = {}) => {
    app.container.set('layout', {pathString, props});
};

const getComponent = (pathString, props = {}) => {
    const paths = app.container.get('paths');
    const filePath = path.join(...pathString.split('.')) + '.js';
    const file = `${paths.app}/Views/${filePath}`;
    const res = app.container.get('res');

    if (!fs.existsSync(file)) {
        res.statusCode = 404;
        return null;
    }

    res.setHeader('Content-Type', 'text/html; charset=utf-8');

    return {
        component: require(file).default,
        props: props
    };
};

const render = (pathString, props = {}) => {
    const component = getComponent(pathString, props);

    if (component === null) {
        return null;
    }

    const layout = app.container.get('layout');

    if (layout !== null) {
        let reactElement = '';
        const layoutComponent = getComponent(layout.pathString, layout.props);

        if (layoutComponent === null) {
            reactElement = createElement(component.component, component.props);
        } else {
            reactElement = createElement(
                layoutComponent.component, layoutComponent.props,
                createElement(component.component, component.props)
            );
        }

        app.container.remove('layout');

        return reactElement;
    }

    return createElement(component.component, component.props);
};

export default {
    layout,
    render
};
