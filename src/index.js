import app from "./app/app";
import route from "./routing/route";
import view from "./view";
import Modal from "./model";

const bootstrap = (config) => {
    app.container.set('paths', config.paths);
};

export {
    route,
    view,
    Modal,
    bootstrap
};

export default app;
