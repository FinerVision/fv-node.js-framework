'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _http = require('http');

var _http2 = _interopRequireDefault(_http);

var _url = require('url');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var handleRequest = function handleRequest(method, options) {
    if (!_http2.default.hasOwnProperty(method)) {
        console.log('Method ' + method + ' is not supported.');
        throw new Error();
    }

    return new Promise(function (reject, resolve) {
        _http2.default[method]({
            host: options.url.hostname,
            path: options.url.pathname
        }, function (response) {
            response.on('error', function () {
                console.log('error');
                reject(response);
            });

            response.on('end', function () {
                console.log('end');
                resolve(response);
            });
        });
    });
};

exports.default = {
    get: function get(url) {
        var options = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {};

        options.url = new _url.URL(url);
        return handleRequest('get', options);
    }
};