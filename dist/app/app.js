"use strict";

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _container = require("./container");

var _container2 = _interopRequireDefault(_container);

var _routing = require("../routing");

var _routing2 = _interopRequireDefault(_routing);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = {
    container: _container2.default,
    routing: _routing2.default
};