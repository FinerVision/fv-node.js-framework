"use strict";

Object.defineProperty(exports, "__esModule", {
    value: true
});
/**
 * This is the app container. All app-wide data is
 * stored here. Any setting and getting should be
 * performed with the set and get methods.
 */
exports.default = {
    routes: {},

    req: null,

    res: null,

    set: function set(key, value) {
        this[key] = value;
    },
    get: function get(key) {
        if (!this.hasOwnProperty(key)) {
            return null;
        }

        return this[key];
    },
    remove: function remove(key) {
        if (this.hasOwnProperty(key)) {
            delete this[key];
        }
    }
};