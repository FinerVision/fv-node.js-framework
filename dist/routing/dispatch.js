"use strict";

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _server = require("react-dom/server");

var _react = require("react");

var _react2 = _interopRequireDefault(_react);

var _app = require("../app/app");

var _app2 = _interopRequireDefault(_app);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

// TODO: Handle errors properly.
exports.default = function (req, res) {
    // Check if requested url is in the app's routes
    if (!_app2.default.container.routes.hasOwnProperty(req.url)) {
        res.statusCode = 404;
        return res.end();
    }

    var route = _app2.default.container.routes[req.url];

    // Check if requested HTTP method is allowed
    if (!route.hasOwnProperty(req.method)) {
        res.statusCode = 405;
        return res.end();
    }

    res.statusCode = 200;

    var response = route[req.method].callable();

    try {
        response = (0, _server.renderToString)(response);
    } catch (e) {
        if (!(response instanceof Buffer)) {
            response = JSON.stringify(response);
        }
    }

    if (response === undefined) {
        response = '';
    }

    res.write(response);

    return res.end();
};