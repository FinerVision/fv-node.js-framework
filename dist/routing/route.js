'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _app = require('../app/app');

var _app2 = _interopRequireDefault(_app);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/**
 * Adds the given route's method, url and action to the
 * app container for future use on an incoming request.
 *
 * @param method
 * @param url
 * @param action
 */
var addRoute = function addRoute(method, url, action) {
    var routes = _app2.default.container.get('routes');
    routes[url] = {};
    routes[url][method] = {};

    if (typeof action === 'string') {
        (function () {
            action = action.split('@');
            var controller = require(_app2.default.container.get('paths').app + ('/Controllers/' + action[0])).default;
            var callable = action[1];
            routes[url][method].callable = function () {
                return new controller()[callable]();
            };
        })();
    }

    if (typeof action === 'function') {
        routes[url][method].callable = function () {
            return action();
        };
    }

    _app2.default.container.set('routes', routes);
};

/**
 * All of the supported HTTP request methods are listed here. You can see a complete list of
 * methods here - https://en.wikipedia.org/wiki/Hypertext_Transfer_Protocol#Request_methods
 */
exports.default = {
    get: function get(url, action) {
        return addRoute('GET', url, action);
    },
    post: function post(url, action) {
        return addRoute('POST', url, action);
    },
    put: function put(url, action) {
        return addRoute('PUT', url, action);
    },
    patch: function patch(url, action) {
        return addRoute('PATCH', url, action);
    },
    delete: function _delete(url, action) {
        return addRoute('DELETE', url, action);
    }
};