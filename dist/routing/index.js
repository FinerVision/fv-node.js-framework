'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _dispatch = require('./dispatch');

var _dispatch2 = _interopRequireDefault(_dispatch);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = {
    dispatch: _dispatch2.default
};