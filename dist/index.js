"use strict";

Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.bootstrap = exports.Modal = exports.view = exports.route = undefined;

var _app = require("./app/app");

var _app2 = _interopRequireDefault(_app);

var _route = require("./routing/route");

var _route2 = _interopRequireDefault(_route);

var _view = require("./view");

var _view2 = _interopRequireDefault(_view);

var _model = require("./model");

var _model2 = _interopRequireDefault(_model);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var bootstrap = function bootstrap(config) {
    _app2.default.container.set('paths', config.paths);
};

exports.route = _route2.default;
exports.view = _view2.default;
exports.Modal = _model2.default;
exports.bootstrap = bootstrap;
exports.default = _app2.default;