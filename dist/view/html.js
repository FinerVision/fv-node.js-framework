"use strict";

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _path = require("path");

var _path2 = _interopRequireDefault(_path);

var _fs = require("fs");

var _fs2 = _interopRequireDefault(_fs);

var _app = require("../app/app");

var _app2 = _interopRequireDefault(_app);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _toConsumableArray(arr) { if (Array.isArray(arr)) { for (var i = 0, arr2 = Array(arr.length); i < arr.length; i++) { arr2[i] = arr[i]; } return arr2; } else { return Array.from(arr); } }

exports.default = function (pathString) {
    var paths = _app2.default.container.get('paths');
    var filePath = _path2.default.join.apply(_path2.default, _toConsumableArray(pathString.split('.'))) + '.html';
    var file = paths.app + ("/Views/" + filePath);
    var res = _app2.default.container.get('res');

    if (!_fs2.default.existsSync(file)) {
        res.statusCode = 404;
        return null;
    }

    res.setHeader('Content-Type', 'text/html; charset=utf-8');

    return _fs2.default.readFileSync(file);
};