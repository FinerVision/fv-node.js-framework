"use strict";

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _react = require("react");

var _path = require("path");

var _path2 = _interopRequireDefault(_path);

var _fs = require("fs");

var _fs2 = _interopRequireDefault(_fs);

var _app = require("../app/app");

var _app2 = _interopRequireDefault(_app);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _toConsumableArray(arr) { if (Array.isArray(arr)) { for (var i = 0, arr2 = Array(arr.length); i < arr.length; i++) { arr2[i] = arr[i]; } return arr2; } else { return Array.from(arr); } }

var layout = function layout(pathString) {
    var props = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {};

    _app2.default.container.set('layout', { pathString: pathString, props: props });
};

var getComponent = function getComponent(pathString) {
    var props = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {};

    var paths = _app2.default.container.get('paths');
    var filePath = _path2.default.join.apply(_path2.default, _toConsumableArray(pathString.split('.'))) + '.js';
    var file = paths.app + "/Views/" + filePath;
    var res = _app2.default.container.get('res');

    if (!_fs2.default.existsSync(file)) {
        res.statusCode = 404;
        return null;
    }

    res.setHeader('Content-Type', 'text/html; charset=utf-8');

    return {
        component: require(file).default,
        props: props
    };
};

var render = function render(pathString) {
    var props = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {};

    var component = getComponent(pathString, props);

    if (component === null) {
        return null;
    }

    var layout = _app2.default.container.get('layout');

    if (layout !== null) {
        var reactElement = '';
        var layoutComponent = getComponent(layout.pathString, layout.props);

        if (layoutComponent === null) {
            reactElement = (0, _react.createElement)(component.component, component.props);
        } else {
            reactElement = (0, _react.createElement)(layoutComponent.component, layoutComponent.props, (0, _react.createElement)(component.component, component.props));
        }

        _app2.default.container.remove('layout');

        return reactElement;
    }

    return (0, _react.createElement)(component.component, component.props);
};

exports.default = {
    layout: layout,
    render: render
};