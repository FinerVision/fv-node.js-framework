"use strict";

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _react = require("./react");

var _react2 = _interopRequireDefault(_react);

var _html = require("./html");

var _html2 = _interopRequireDefault(_html);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = {
    react: _react2.default,
    html: _html2.default
};